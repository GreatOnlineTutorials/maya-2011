//对前臂骨骼进行切换到FK
float $ikjnt1rx = `getAttr ik_jnt_1.rx`;
float $ikjnt1ry = `getAttr ik_jnt_1.ry`;
float $ikjnt1rz = `getAttr ik_jnt_1.rz`;

setAttr "fk_jnt_1.rotateZ" $ikjnt1rz;
setAttr "fk_jnt_1.rotateX" $ikjnt1rx;
setAttr "fk_jnt_1.rotateY" $ikjnt1ry;

//对第二段骨骼进行切换到FK
float $ikjnt2ry =`getAttr ik_jnt_2.ry`;
setAttr "B.rotateY" $ikjnt2ry;

//把开关设置成FK
setAttr "IKFK_cv_ctr.IKFK" 1;