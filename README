##     ##    ###   ##    ##   ###      #######    #####      ##     ##  
###   ###   ## ##   ##  ##   ## ##    ##     ##  ##   ##   ####   ####  
#### ####  ##   ##   ####   ##   ##          ## ##     ##    ##     ##  
## ### ## ##     ##   ##   ##     ##   #######  ##     ##    ##     ##  
##     ## #########   ##   #########  ##        ##     ##    ##     ##  
##     ## ##     ##   ##   ##     ##  ##         ##   ##     ##     ##  
##     ## ##     ##   ##   ##     ##  #########   #####    ###### ######
=========================== MAYA 2011 白金手册 ===========================

原本各DVD里面的可放进移动设备上播放的MP4视频已删除，因为DVD 06和12都只有MP4，
所以没有其他文件。所有视频都已经上传到YouTube，包括部分Scene文件夹内的素材
视频，而网页上的原本指向这些视频的链接都换成YouTube链接。你可以根据不同分支
（branch）下载不包含scene文件夹的网页版或者完整版，但这些都不会包含原本19只
DVD上的大小超过10M的视频。

基本结构如下：

第1册基础 2DVD 01-02
第2册建模 4DVD 03-06
第3册材质、灯光与渲染 3DVD 07-09
第4册mental ray渲染 3DVD 10-12
第5册动画 3DVD 13-15
第6册特效 4DVD 16-19


简介
====

《火星人——Maya白金手册》是一套全面学习并使用Maya软件制作三维动画的大型多媒
体教学手册，它对Maya的强大功能做了详尽的讲解。本套丛书共分6册：基础，建模，
材质、 灯光与渲染，Mentalray渲染，动画与特效。 本册是第6册，即特效，内容包
括Particles（粒子）、nParticles（n粒子）、Fluid Effects （流体特效）、Fluid
nCache（流体n缓存）、Fields（场）、Soft/Rigid Bodies（柔体/刚体）、 Effects
（特效）、Solvers（解算器）、nSolver（n解算器）、Hair（头发）、nMesh（n网
格）、nConstraint（n约束）等，详细讲解了与特效制作相关的各菜单命令的使用方
法与参数设 置，方便读者学习和查阅。 随书附带4张DVD多媒体教学光盘，教学视频
时间长达20多个小时，内含Maya特效基础及 书中大部分实例的操作过程演示；素材
包括读者学习本手册过程中需要使用到的场景及相关素 材文件。本套手册不仅适合
Maya初中级读者阅读，也可以作为高等院校三维动画设计相关专业的教 辅图书及相关
教师的参考图书。


第1册基础
=========
第1章 Maya概述
	主要讲解Maya应用领域、安装方法及功能介绍。

第2章 创作第一个动画
	主要讲解创作一个小球动画的全过程。

第3章 Maya基本操作
	主要讲解Maya中的文件操作、变换工具的使用、编辑操作、视图控制等。

第4章 Maya界面详解
	主要讲解Maya界面中各个部分的介绍与使用。

第5章 公共菜单详解
	主要讲解Maya中File（文件）、Edit（编辑）、Modify（修改）、Create
	（创建）、Display（显示）、Window（窗口）菜单的使用。

第6章 Maya软件设置
	主要讲解Maya中参数、工具、性能、热键、面板以及插件管理等设置。

第7章 Maya动画项目流程
	主要讲解在Maya中进行项目制作与管理的流程和方法。


第2册建模
=========
第1章 Maya建模基础
	主要讲解多边形建模、NURBS建模和Subdiv（细分）建模的概念及其特点，
	并以一个酒杯模型的制作开启学习之旅。

第2章 创建NURBS基本对象
	主要讲解NURBS曲线及曲面的创建。

第3章 编辑NURBS曲线
	主要讲解各NURBS曲线编辑工具的应用及参数说明。

第4章 NURBS曲面创建方式
	主要讲解各NURBS曲面创建工具的应用及参数说明。

第5章 创建多边形几何体
	主要讲解多边形创建工具的应用及参数说明。

第6章 多边形四大组件
	主要讲解Vertex（顶点）、Edge（边）、Face（面）、UV（UV点）和Vertex
	Face（顶点面）等编辑模式下各个命令的应用及参数说明。

第7章 Mesh（网格）菜单
	主要讲解Mesh（网格）菜单中各命令的应用及参数说明。

第8章 Edit Mesh（编辑网格）
	主要讲解Edit Mesh（编辑网格）菜单中各命令的应用及参数说明。

第9章 Proxy（代理）对象
	主要讲解Proxy（代理）菜单中各命令的应用及参数说明。

第10章 多边形Normals（法线）
	主要讲解Normals（法线）菜单中各命令的应用及参数说明。

第11章 Color（颜色）菜单
	主要讲解Color（颜色）菜单中常用命令的应用及参数说明。

第12章 Create Uvs（创建UV点）菜单
	主要讲解Create Uvs（创建UV点）菜单中常用命令的应用及参数说明。

第13 章 Edit Uvs（编辑Uvs）菜单
	主要讲解Edit Uvs（编辑Uvs）菜单中常用命令的应用及参数说明。

第14 章 Subdiv（细分）建模
	主要讲解Subdiv（细分）建模工具及编辑工具的使用方法。


第3册材质、灯光与渲染
=====================
第1章 Maya材质基础
	主要讲解Maya材质、灯光与渲染的概念，以实例的形式介绍玻璃材质效果的
	表现方法。

第2章 材质编辑器
	主要讲解Hypershade（材质编辑器）窗口中各菜单命令、工具的使用方法及
	参数说明。

第3章 材质着色器
	主要讲解Hypershade（材质编辑器）窗口中Surface Shader（表面着色器）、
	Volumetric Shader（体积着色器）、Displacement（置换）等常用着色器的
	使用方法。

第4章 Textures程序纹理
	主要讲解Hypershade（材质编辑器）窗口中Texture（2D纹理）、3D
	Textures（3D纹理）和Env Textures（环境纹理）等Maya基础纹理的应用。

第5章 Utilities工具节点
	主要讲解Hypershade（材质编辑器）窗口中General Utilities（常规工具）、
	Color Utilities（颜色工具）和Switch Utilities（转换工具）等节点的应用。

第6章 Light（灯光）
	主要讲解Maya灯光类型、Maya 灯光基本属性、Maya灯光的链接等内容。

第7章 Rendering（渲染）
	主要讲解Render View（渲染视图）窗口、渲染类型、渲染设置通用面板、
	渲染的其他属性设置、渲染层等内容。

第8章 渲染模块菜单
	主要讲解Lighting/Shading（灯光/材质）、Texturing（纹理）、Render
	（渲染）、Toon（卡通）、Stereo（立体）、Paint Effects（画笔特效）、
	Fur（毛发）等菜单中各命令的应用及参数说明。


第4册mental ray渲染
===================
第1章 Mentalray介绍
	主要讲解Mentalray基础知识，包括Mentalray基本概念、加载Mentalray、材
	质定义、Scanline渲染和Raytracing渲染、全局照明等内容。

第2章 Indirect Lighting( 间接照明）
	主要讲解Mentalray渲染器中焦散、全局照明和最终聚焦的基本参数设置，
	包括Environment（环境） 、 Global Illumination（全局照明） 、
	Caustics（焦散） 、 Photon Tracing（光子追踪） 、 Photon Map（光子
	贴图） 、 Photon Volume（光子体积） 、Importons、 Final Gathering
	（最终聚焦） 、 Irradiance Particles（发光粒子） 等内容。

第3章 材质
	主要讲解Mentalray材质的参数设置与使用，包括表面材质Shader、Sub-
	Surface Scattering（次表面散射）、阴影Shader、光子Shader、光子体积
	Shader、纹理Shader、Light Maps（灯光贴图）、Lenses (镜头） 、
	Data Conversion (数据转换） 、Geometry（几何体）和Miscellaneous
	（多样化） 等内容。

第4章 渲染设置
	主要讲解Mentalray的渲染设置，包括Scanline（扫描线）渲染器、
	Rasterizer（Rapid Motion） ［栅格化（快速运动）］ 渲染器 、
	Raytracing（光线跟踪） 渲染器、 Render Passes（渲染通道） 、
	Pre-Compositing（预合成） 等内容。

第5章 Motion Blur（运动模糊）
	主要讲解渲染设置中Motion Blur（运动模糊）的应用及参数说明。

第6章 mental ray 参数设置
	主要讲解Features（特性）、Quality（质量）、Anti-Aliasing Quality
	（抗锯齿质量）、Raytracing（光线追踪）、Rasterizer（栅格化）、
	Shadow（阴影）和Framebuffer（帧缓冲）等参数的设置。


第5册动画
=========
第1章 动画基础知识介绍
	主要讲解动画原理、进行动画制作的准备工作，并通过人物行走动画的制作
	来介绍角色动画制作的基本流程和方法。

第2章 动画控制
	主要讲解帧、帧率和帧率设置，时间滑块、时间范围滑块和播放控制器，动
	画控制菜单，预览动画等内容。

第3章 Graph Editor（曲线编辑器）
	主要讲解曲线编辑器中各种动画工具的应用及参数说明。

第4章 Animate（动画） 菜单
	主要讲解Animate（动画）菜单中各命令的应用及参数说明。

第5章 Geometry Cache（几何体缓存）菜单
	主要讲解Geometry Cache（几何体缓存）菜单中各命令的应用及参数说明。

第6章 Create Deformers（创建变形器）菜单
	主要讲解Create Deformers（创建变形器）菜单中各个命令的应用及参数说明。

第7章 Edit Deformers（编辑变形器）菜单
	主要讲解Edit Deformers（编辑变形器）菜单中各命令的应用及参数说明。

第8章 Skeleton（骨骼）菜单
	主要讲解Skeleton（骨骼） 菜单中各命令的应用及参数说明。

第9章 Skin（蒙皮） 菜单
	主要讲解Skin（蒙皮） 菜单中各命令的应用及参数说明。

第10章 Constrain（约束） 菜单
	主要讲解Constrain（约束） 菜单中各命令的应用及参数说明。

第11章 Character（角色） 菜单
	主要讲解Character（角色） 菜单中各命令的应用及参数说明。

第12章 Muscle（肌肉） 菜单
	主要讲解Muscle（肌肉） 菜单菜单中各命令的应用及参数说明。

第13章 Animation Layer（动画层） 编辑器
	主要讲解Animation Layer（动画层） 中各命令的应用及参数说明。

第14章 Animation Editors（动画编辑器）
	主要讲解Animation Editors（动画编辑器）中各命令工具的应用及参数说明。


第6册特效
=========
第1章 特效基础知识
	主要讲解特效基础知识，包括特效相关菜单模块介绍、动画与动力学、设置
	动力学的初始状态、工作与动力学动画run-up等内容，最后通过一个爆炸特
	效的制作来带领读者熟悉特效的基本制作流程。

第2章 Particles/nParticles（粒子/n粒子）菜单
	主要讲解Particles（粒子）和nParticles（n粒子）的创建与参数设置。

第3章 Fluid Effects（流体特效）菜单
	主要讲解Fluid Effects（流体特效）菜单中各命令工具的应用与参数说明。

第4章 Fluid nCache/nCache（流体n缓存/n缓存）菜单
	主要讲解Fluid nCache/nCache（流体n缓存/n缓存）菜单中各命令的应用及
	参数说明。

第5章 Fields（场）菜单
	主要讲解Fields（场）菜单中各命令的应用及参数说明。

第6章 Soft/Rigid Bodies（柔体/刚体）菜单
	主要讲解Soft/Rigid Bodies（柔体/刚体）菜单中各个命令的应用及参数说明。

第7章 Effects（特效）菜单
	主要讲解Effects（特效）菜单中各命令的应用及参数说明。

第8章 Solvers/nSolvers（解算器/n解算器）菜单
	主要讲解Solvers/nSolvers（解算器/n解算器）菜单中各命令的应用及参数
	说明。

第9章 Hair（头发）菜单
	主要讲解Hair（头发）菜单中各命令的应用及参数说明。

第10章 nMesh（n 网格）菜单
	主要讲解nMesh（n 网格）菜单中各命令的应用及参数说明。

第11章 nConstraint（n 约束）菜单与Dynamic Relationships（ 动力学关系）
	主要讲解nConstraint（n 约束）菜单中各命令的应用及参数说明，以及
	Dynamic Relationships Editor（动力学关系编辑器）的使用。


Copyright
=========

北京火星时代科技有限公司出品

网址：http://www.hxsd.com

光盘策划：火星时代
文本编著：火星时代
软件开发：火星时代
出    版：人民邮电出版社
责任编辑信箱：guofaming@ptpress.com.cn
人民邮电出版社网址：http://www.ptpress.com.cn

